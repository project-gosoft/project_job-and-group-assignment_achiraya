package com.todolist;

import java.util.ArrayList;
import java.util.Scanner;

public class TodoList {
	static ArrayList<String> task = new ArrayList<String>();
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println(
				"Please enter the number according to the menu you want. \n1. Add \n2. Edit \n3. Delete \n4. View");
		System.out.print("The menu you choose is : ");

		int MenuNumbers = input.nextInt();
		switch (MenuNumbers) {
		case 1:
			Task.Todo(task);
			Task.Add(task);

			break;
		case 2:
			Task.Todo(task);
			Task.Edit(task);

			break;
		case 3:
			Task.Todo(task);
			Task.Delete(task);

			break;
		case 4:
			Task.Todo(task);
			Task.View(task);

			break;
		}
	}
}
